export const COLOR = {
	PRIMARY_MAIN: '#0C2F54',
	PRIMARY_LIGHT: '#3c5876',
	PRIMARY_DARK: '#08203a',
	SECONDARY_MAIN: '#06563c',
	SECONDARY_LIGHT: '#377763',
	SECONDARY_DARK: '#377763',
	WHITE: '#fff',
	WHITE_87: 'rgba(255, 255, 255, 0.87)',
	BLACK: '#000',
	BLACK_6: 'rgba(0, 0, 0, .06)',
	BLACK_20: 'rgba(0, 0, 0, .2)',
	BLACK_30: 'rgba(0, 0, 0, .3)',
	BLACK_40: 'rgba(0, 0, 0, .4)',
	BLACK_50: 'rgba(0, 0, 0, .5)',
	BLACK_60: 'rgba(0, 0, 0, .6)',
	GRAY: '#f0f0f0'
};
