/**
 *  Copyright (C) 2017 3D Repo Ltd
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import { put, takeLatest } from 'redux-saga/effects';

import * as API from '../../services/api';
import { getAngularService, dispatch } from './../../helpers/migration';
import { uploadFileStatuses } from './model.helpers';
import { DialogActions } from '../dialog';
import { ModelTypes, ModelActions } from './model.redux';
import { TeamspacesActions } from '../teamspaces';
import { SnackbarActions } from './../snackbar';

export function* fetchSettings({ teamspace, modelId }) {
	try {
		yield put(ModelActions.setPendingState(true));

		const { data: settings } = yield API.getModelSettings(teamspace, modelId);

		yield put(ModelActions.fetchSettingsSuccess(settings));
		yield put(ModelActions.setPendingState(false));
	} catch (e) {
		yield put(DialogActions.showErrorDialog('fetch', 'model settings', e.response));
	}
}

export function* updateSettings({ modelData: { teamspace, project, modelId }, settings }) {
	try {
		yield API.editModelSettings(teamspace, modelId, settings);

		yield put(TeamspacesActions.updateModelSuccess(
			teamspace, modelId, { project, model: modelId, name: settings.name } ));
		yield put(SnackbarActions.show('Updated model settings'));
	} catch (e) {
		yield put(DialogActions.showErrorDialog('update', 'model settings', e.response));
	}
}

export function* fetchRevisions({ teamspace, modelId }) {
	try {
		yield put(ModelActions.setPendingState(true));

		const { data: revisions } = yield API.getModelRevisions(teamspace, modelId);

		yield put(ModelActions.fetchRevisionsSuccess(revisions));
		yield put(ModelActions.setPendingState(false));
	} catch (e) {
		yield put(DialogActions.showErrorDialog('fetch', 'model revisions', e.response));
	}
}

export function* downloadModel({ teamspace, modelId }) {
	try {
		const url = yield API.getAPIUrl(`${teamspace}/${modelId}/download/latest`);
		window.open(url, '_blank');
	} catch (e) {
		yield put(DialogActions.showErrorDialog('download', 'model', e.response));
	}
}

export function* onModelStatusChanged({ modelData, teamspace, project, modelId, modelName }) {
	yield put(TeamspacesActions.setModelUploadStatus(teamspace, project, modelId, modelData));

	if (modelData.status === uploadFileStatuses.ok) {
		yield put(SnackbarActions.show(`Model ${modelName} uploaded successfully`));
	}
	if (modelData.status === uploadFileStatuses.failed) {
		if (modelData.hasOwnProperty('errorReason') && modelData.errorReason.message) {
			yield put(SnackbarActions.show(`Failed to import ${modelName} model: ${modelData.errorReason.message}`));
		} else {
			yield put(SnackbarActions.show(`Failed to import ${modelName} model`));
		}
	}
}

export function* subscribeOnStatusChange({ teamspace, project, modelData }) {
	const { modelId, modelName } = modelData;
	const notificationService = yield getAngularService('ChatService');
	const modelNotifications = yield notificationService.getChannel(teamspace, modelId).model;

	const onChanged = (changedModelData) =>
		dispatch(ModelActions.onModelStatusChanged(changedModelData, teamspace, project, modelId, modelName));
	modelNotifications.subscribeToStatusChanged(onChanged, this);
}

export function* unsubscribeOnStatusChange({ teamspace, project, modelData }) {
	const { modelId, modelName } = modelData;
	const notificationService = yield getAngularService('ChatService');
	const modelNotifications = yield notificationService.getChannel(teamspace, modelId).model;

	const onChanged = (changedModelData) =>
		dispatch(ModelActions.onModelStatusChanged(changedModelData, teamspace, project,  modelId, modelName));
	modelNotifications.unsubscribeFromStatusChanged(onChanged, this);
}

export function* uploadModelFile({ teamspace, project, modelData, fileData }) {
	try {
		const formData = new FormData();
		formData.append('file', fileData.file);
		formData.append('tag', fileData.tag);
		formData.append('desc', fileData.desc);

		const { modelId, modelName } = modelData;
		const { data: { status }, data } = yield API.uploadModelFile(teamspace, modelId, formData);

		if (status === uploadFileStatuses.ok) {
			if (data.hasOwnProperty('errorReason') && data.errorReason.message) {
				yield put(SnackbarActions.show(data.errorReason.message));
			} else {
				yield put(SnackbarActions.show(`Model ${modelName} uploaded successfully`));
			}
		}
		if (status === uploadFileStatuses.failed) {
			if (data.hasOwnProperty('errorReason') && data.errorReason.message) {
				yield put(SnackbarActions.show(`Failed to import ${modelName} model: ${data.errorReason.message}`));
			} else {
				yield put(SnackbarActions.show(`Failed to import ${modelName} model`));
			}
		}
	} catch (e) {
		yield put(DialogActions.showErrorDialog('upload', 'model', e.response));
		yield put(TeamspacesActions.setModelUploadStatus(teamspace, project, modelData.modelId, uploadFileStatuses.failed));
	}
}

export default function* ModelSaga() {
	yield takeLatest(ModelTypes.FETCH_SETTINGS, fetchSettings);
	yield takeLatest(ModelTypes.UPDATE_SETTINGS, updateSettings);
	yield takeLatest(ModelTypes.FETCH_REVISIONS, fetchRevisions);
	yield takeLatest(ModelTypes.DOWNLOAD_MODEL, downloadModel);
	yield takeLatest(ModelTypes.UPLOAD_MODEL_FILE, uploadModelFile);
	yield takeLatest(ModelTypes.ON_MODEL_STATUS_CHANGED, onModelStatusChanged);
	yield takeLatest(ModelTypes.SUBSCRIBE_ON_STATUS_CHANGE, subscribeOnStatusChange);
	yield takeLatest(ModelTypes.UNSUBSCRIBE_ON_STATUS_CHANGE, unsubscribeOnStatusChange);
}
